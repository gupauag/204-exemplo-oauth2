package br.com.OauthCarro;

import br.com.OauthCarro.security.Usuario;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;

@RestController
public class ControlleController {

    @GetMapping("/{modelo}")                            // mudamos o principal para retorno um usuario completo
    public Carro getCarro(@PathVariable String modelo, @AuthenticationPrincipal Usuario usuario){

        Carro carro = new Carro();
        carro.setModelo(modelo);
        carro.setPlaca("ABC");
        carro.setUsuario(usuario);

        return carro;
    }

}

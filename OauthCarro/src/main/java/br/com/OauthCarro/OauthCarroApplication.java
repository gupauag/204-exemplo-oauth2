package br.com.OauthCarro;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OauthCarroApplication {

	public static void main(String[] args) {
		SpringApplication.run(OauthCarroApplication.class, args);
	}

}

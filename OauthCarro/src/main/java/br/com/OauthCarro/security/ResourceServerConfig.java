package br.com.OauthCarro.security;

import org.springframework.boot.autoconfigure.security.oauth2.resource.PrincipalExtractor;
import org.springframework.boot.autoconfigure.security.reactive.ReactiveSecurityAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;

import java.util.Map;

@Configuration
@EnableResourceServer
public class ResourceServerConfig extends ResourceServerConfigurerAdapter {

    @Override
    public void configure(HttpSecurity http) throws Exception{

        //permite todas as requisições e nem se liga para nada de segurança;
//        http.authorizeRequests().anyRequest().permitAll();

        // só permite acesso para quem tem permissão configurada em uma rota no properties
        http.authorizeRequests().anyRequest().authenticated();

    }

    @Bean
    public PrincipalExtractor getPrincipalExtractor(){
        return new UsuarioPrincipalExtractor();
    }

}

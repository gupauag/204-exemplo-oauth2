package com.poc.authserver.usuario;

import java.util.Collections;
import java.util.Optional;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class UsuarioService implements UserDetailsService {
  @Autowired
  private BCryptPasswordEncoder encoder;
  
  @Autowired
  private UsuarioRepository repository;
  
  @PostConstruct
  public void popular() {
    Usuario usuario = new Usuario();
    usuario.setNome("nicao");
    usuario.setSenha(encoder.encode("nicao123"));
    repository.save(usuario);

    Usuario alanAugusto = new Usuario();
    alanAugusto.setNome("alan-augusto");
    alanAugusto.setSenha("alan-augusto123");
    repository.save(alanAugusto);

    Usuario alanNigri = new Usuario();
    alanNigri.setNome("alan-nigri");
    alanNigri.setSenha("alan-nigri123");
    repository.save(alanNigri);

    Usuario alderRienes = new Usuario();
    alderRienes.setNome("alder-rienes");
    alderRienes.setSenha("alder-rienes123");
    repository.save(alderRienes);

    Usuario amandaFutemma = new Usuario();
    amandaFutemma.setNome("amanda-futemma");
    amandaFutemma.setSenha("amanda-futemma123");
    repository.save(amandaFutemma);

    Usuario anaCarolina = new Usuario();
    anaCarolina.setNome("ana-carolina");
    anaCarolina.setSenha("ana-carolina123");
    repository.save(anaCarolina);

    Usuario andressaAyumi = new Usuario();
    andressaAyumi.setNome("andressa-ayumi");
    andressaAyumi.setSenha("andressa-ayumi123");
    repository.save(andressaAyumi);

    Usuario arthurJorge = new Usuario();
    arthurJorge.setNome("arthur-jorge");
    arthurJorge.setSenha("arthur-jorge123");
    repository.save(arthurJorge);

    Usuario brunoMagnum = new Usuario();
    brunoMagnum.setNome("bruno-magnum");
    brunoMagnum.setSenha("bruno-magnum123");
    repository.save(brunoMagnum);

    Usuario brunoMancini = new Usuario();
    brunoMancini.setNome("bruno-mancini");
    brunoMancini.setSenha("bruno-mancini123");
    repository.save(brunoMancini);

    Usuario carolinaSilva = new Usuario();
    carolinaSilva.setNome("carolina-silva");
    carolinaSilva.setSenha("carolina-silva123");
    repository.save(carolinaSilva);

    Usuario daniloGouveia = new Usuario();
    daniloGouveia.setNome("danilo-gouveia");
    daniloGouveia.setSenha("danilo-gouveia123");
    repository.save(daniloGouveia);

    Usuario diegoRosendo = new Usuario();
    diegoRosendo.setNome("diego-rosendo");
    diegoRosendo.setSenha("diego-rosendo123");
    repository.save(diegoRosendo);

    Usuario douglasMaldonado = new Usuario();
    douglasMaldonado.setNome("douglas-maldonado");
    douglasMaldonado.setSenha("douglas-maldonado123");
    repository.save(douglasMaldonado);

    Usuario eduardoPereira = new Usuario();
    eduardoPereira.setNome("eduardo-pereira");
    eduardoPereira.setSenha("eduardo-pereira123");
    repository.save(eduardoPereira);

    Usuario fernandoJacyntho = new Usuario();
    fernandoJacyntho.setNome("fernando-jacyntho");
    fernandoJacyntho.setSenha("fernando-jacyntho123");
    repository.save(fernandoJacyntho);

    Usuario giovanniVicente = new Usuario();
    giovanniVicente.setNome("giovanni-vicente");
    giovanniVicente.setSenha("giovanni-vicente123");
    repository.save(giovanniVicente);

    Usuario gustavoAguiar = new Usuario();
    gustavoAguiar.setNome("gustavo-aguiar");
    gustavoAguiar.setSenha("gustavo-aguiar123");
    repository.save(gustavoAguiar);

    Usuario gustavoMartinelli = new Usuario();
    gustavoMartinelli.setNome("gustavo-martinelli");
    gustavoMartinelli.setSenha("gustavo-martinelli123");
    repository.save(gustavoMartinelli);

    Usuario henriqueKostriuba = new Usuario();
    henriqueKostriuba.setNome("henrique-kostriuba");
    henriqueKostriuba.setSenha("henrique-kostriuba123");
    repository.save(henriqueKostriuba);

    Usuario joseJefferson = new Usuario();
    joseJefferson.setNome("jose-jefferson");
    joseJefferson.setSenha("jose-jefferson123");
    repository.save(joseJefferson);

    Usuario julioFelipe = new Usuario();
    julioFelipe.setNome("julio-felipe");
    julioFelipe.setSenha("julio-felipe123");
    repository.save(julioFelipe);

    Usuario leandroGuarino = new Usuario();
    leandroGuarino.setNome("leandro-guarino");
    leandroGuarino.setSenha("leandro-guarino123");
    repository.save(leandroGuarino);

    Usuario marceleMenezes = new Usuario();
    marceleMenezes.setNome("marcele-menezes");
    marceleMenezes.setSenha("marcele-menezes123");
    repository.save(marceleMenezes);

    Usuario marcoAurelio = new Usuario();
    marcoAurelio.setNome("marco-aurelio");
    marcoAurelio.setSenha("marco-aurelio123");
    repository.save(marcoAurelio);

    Usuario patriciaCardoso = new Usuario();
    patriciaCardoso.setNome("patricia-cardoso");
    patriciaCardoso.setSenha("patricia-cardoso123");
    repository.save(patriciaCardoso);

    Usuario patriciaNovaes = new Usuario();
    patriciaNovaes.setNome("patricia-novaes");
    patriciaNovaes.setSenha("patricia-novaes123");
    repository.save(patriciaNovaes);

    Usuario pauloCesar = new Usuario();
    pauloCesar.setNome("paulo-cesar");
    pauloCesar.setSenha("paulo-cesar123");
    repository.save(pauloCesar);

    Usuario pedroDorighello = new Usuario();
    pedroDorighello.setNome("pedro-dorighello");
    pedroDorighello.setSenha("pedro-dorighello123");
    repository.save(pedroDorighello);

    Usuario rafaelAntonio = new Usuario();
    rafaelAntonio.setNome("rafael-antonio");
    rafaelAntonio.setSenha("rafael-antonio123");
    repository.save(rafaelAntonio);

    Usuario robsonRigatto = new Usuario();
    robsonRigatto.setNome("robson-rigatto");
    robsonRigatto.setSenha("robson-rigatto123");
    repository.save(robsonRigatto);

    Usuario sauloAroni = new Usuario();
    sauloAroni.setNome("saulo-aroni");
    sauloAroni.setSenha("saulo-aroni123");
    repository.save(sauloAroni);

    Usuario thaisMelo = new Usuario();
    thaisMelo.setNome("thais-melo");
    thaisMelo.setSenha("thais-melo123");
    repository.save(thaisMelo);

    Usuario viniciusFantinatti = new Usuario();
    viniciusFantinatti.setNome("vinicius-fantinatti");
    viniciusFantinatti.setSenha("vinicius-fantinatti123");
    repository.save(viniciusFantinatti);

    Usuario willianGarcia = new Usuario();
    willianGarcia.setNome("willian-garcia");
    willianGarcia.setSenha("willian-garcia123");
    repository.save(willianGarcia);
  }

  @Override
  public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
    Optional<Usuario> optional = repository.findByNome(username);
    
    if(!optional.isPresent()) {
      throw new UsernameNotFoundException("Usuário não encontrado");
    }
    
    Usuario usuario = optional.get();
    
    SimpleGrantedAuthority authority = new SimpleGrantedAuthority("user");
    
    return new User(usuario.getNome(), usuario.getSenha(), Collections.singletonList(authority));
  }
}
